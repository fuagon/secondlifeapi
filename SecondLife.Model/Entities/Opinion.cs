﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SecondLifeModel.Entities
{
    /// <summary>
    /// Représente l'objet qui permettra à un User, d'attribuer une note à un autre avec qui il a interagi
    /// </summary>
    public class Opinion
    {
        [Key]
        public int Id { get; set; }

        public User RequestingUser  { get; set; }

        public User TargetedUser { get; set; }

        public string Comment { get; set; }

        [Range(0,5)]
        public int Note { get; set; }
    }
}