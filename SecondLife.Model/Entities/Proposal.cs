﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SecondLifeModel.Entities
{
    /// <summary>
    /// Représente une proposition d'un acheteur sur une Offer
    /// </summary>
    public class Proposal
    {
        [Key]
        public int Id { get; set; }

        public Offer Offer { get; set; }

        //Utilisateur à l'origine de la proposition
        public User RequestingUser { get; set; }

        //Produits proposés pour un échange
        public ICollection<Product> ProposedProducts { get; set; }

        //Proposition d'achet du proposeur d'offre
        public float ProposedPrice { get; set; }

        //État de la proposition (acceptée, déclinée, annulée, etc.)
        public ProposalState ProposalState { get; set; }

        //Commentaire du vendeur pour préciser la raison de l'acceptation/refus de la proposition
        public string SellerComment { get; set; }
    }
}