﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SecondLifeModel.Entities
{
    /// <summary>
    /// Représente une offre mis en ligne par un User
    /// </summary>
    public class Offer
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        //Prix de base demandé pour le lot
        public float BasePrice { get; set; }

        //Liste des produits mis en vente par le vendeur
        public ICollection<Product> Products { get; set; }

        [JsonIgnore]
        public Opinion Opinion { get; set; }

        //L'utilisateur à qui appartient l'offre
        [JsonIgnore]
        public User Seller { get; set; }

        //L'utilisateur à qui revient l'offre après acceptation de sa proposition
        [JsonIgnore]
        public User Buyer { get; set; }

        //Liste des propositions effectuées par les acheteurs
        [JsonIgnore]
        public ICollection<Proposal> Proposals { get; set; }
    }
}   