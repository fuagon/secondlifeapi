﻿using System.ComponentModel.DataAnnotations;

namespace SecondLifeModel.Entities
{
    /// <summary>
    /// Représente un des produits appartenant à une offre existante ou clôturée
    /// </summary>
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public string PictureUrl { get; set; }

        //Un produit correspond à une fiche "Game"
        public Game Game { get; set; }
    }
}