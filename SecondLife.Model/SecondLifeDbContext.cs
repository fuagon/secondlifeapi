﻿using Microsoft.EntityFrameworkCore;
using SecondLifeModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondLifeModel
{
    public class SecondLifeDbContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Opinion> Opinions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Proposal> Proposals { get; set; }
        public DbSet<User> Users { get; set; }

        public SecondLifeDbContext(DbContextOptions<SecondLifeDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(u => u.SelledOffers)
                .WithOne(e => e.Seller);

            modelBuilder.Entity<User>()
                .HasMany(u => u.BoughtOffers)
                .WithOne(e => e.Buyer);

            modelBuilder
            .Entity<Proposal>()
            .Property(e => e.ProposalState)
            .HasConversion<int>();

            modelBuilder.Ignore<Type>();

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(
                    "server=localhost;port=3306;database=secondlife;uid=dev;password=dev;TreatTinyAsBoolean=false");
            }
            base.OnConfiguring(optionsBuilder);
        }
    }
}
