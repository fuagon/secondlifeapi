﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SecondLifeModel.Migrations
{
    public partial class TestEnumeration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proposals_ProposalStates_ProposalStateId",
                table: "Proposals");

            migrationBuilder.DropTable(
                name: "ProposalStates");

            migrationBuilder.DropIndex(
                name: "IX_Proposals_ProposalStateId",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ProposalStateId",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ShortDescription",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "ProposalState",
                table: "Proposals",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProposalState",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "ProposalStateId",
                table: "Proposals",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortDescription",
                table: "Products",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProposalStates",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProposalStates", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Proposals_ProposalStateId",
                table: "Proposals",
                column: "ProposalStateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Proposals_ProposalStates_ProposalStateId",
                table: "Proposals",
                column: "ProposalStateId",
                principalTable: "ProposalStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
