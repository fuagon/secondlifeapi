﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SecondLifeModel.Migrations
{
    public partial class AddOpinionObjectToOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Opinions_Offers_OfferId",
                table: "Opinions");

            migrationBuilder.DropIndex(
                name: "IX_Opinions_OfferId",
                table: "Opinions");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "Opinions");

            migrationBuilder.AddColumn<int>(
                name: "OpinionId",
                table: "Offers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offers_OpinionId",
                table: "Offers",
                column: "OpinionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Opinions_OpinionId",
                table: "Offers",
                column: "OpinionId",
                principalTable: "Opinions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Opinions_OpinionId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_OpinionId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OpinionId",
                table: "Offers");

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "Opinions",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Opinions_OfferId",
                table: "Opinions",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_Opinions_Offers_OfferId",
                table: "Opinions",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
