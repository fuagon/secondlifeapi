using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Service;
using SecondLife.Service.Services;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.Tests
{
    [TestClass]
    public class OfferControllerTests
    {
        private OfferCrudController _controller;
        private UserCrudController _userController;
        private Mock<IService<Offer>> _offerServiceMock;
        private Mock<IService<User>> _userServiceMock;

        public OfferControllerTests()
        {
            _offerServiceMock = new Mock<IService<Offer>>();
            _userServiceMock = new Mock<IService<User>>();
            _controller = new OfferCrudController(_offerServiceMock.Object);
            _userController = new UserCrudController(_userServiceMock.Object);
        }

        //Un GET Offer avec un id � 0 doit renvoyer une erreur
        [TestMethod]
        public void Get_With0_ThenGetFromRepoIsNotCalled()
        {
            _controller.Get(0);
            _offerServiceMock.Verify(x => x.Get(0), Times.Never());
        }

        //Un GET Offer avec un id � 1 doit renvoyer un r�sultat
        [TestMethod]
        public void Get_With1_ThenGetFromRepoIsCalledWith1()
        {
            _controller.Get(1);
            _offerServiceMock.Verify(x => x.Get(1), Times.Once());
        }

        //La cr�ation d'un Offer sans param�tres doit �chouer
        [TestMethod]
        public void Add_WithNoParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new Offer());
            _offerServiceMock.Verify(x => x.Add(It.IsAny<Offer>()), Times.Never());
        }

        //La cr�ation d'un Offer avec des param�tres manquants doit �chouer
        [TestMethod]
        public void Add_WithMissingParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new Offer()
            {
                Title = "Offre",
                Description = "Description de l'offre"
            });

            _offerServiceMock.Verify(x => x.Add(It.IsAny<Offer>()), Times.Never());
        }

        //La cr�ation d'un Offer avec tous les param�tres n�cessaires doit r�ussir
        [TestMethod]
        public void Add_WithAllParameters_ThenAddIsCalled()
        {
            _controller.Create(new Offer()
            {
                Title = "Offre",
                Description = "Description de l'offre",
                BasePrice = 12,
                Seller = new User() { Id = 1 }
            });

            _offerServiceMock.Verify(x => x.Add(It.IsAny<Offer>()), Times.Once());
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID inexistant doit �chouer
        [TestMethod]
        public void DeleteWithUnknownId_ThenDeleteIsNotOk()
        {
            _offerServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(false);

            var deletedOffer = _controller.Delete(55);

            Assert.IsInstanceOfType(deletedOffer, typeof(NotFoundResult));
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID existant doit r�ussir
        [TestMethod]
        public void DeleteWithExistingId_ThenDeleteIsOk()
        {
            _offerServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new Offer());
            _offerServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(true);

            var deletedOffer = _controller.Delete(1);

            Assert.IsInstanceOfType(deletedOffer, typeof(OkResult));
        }
    }
}