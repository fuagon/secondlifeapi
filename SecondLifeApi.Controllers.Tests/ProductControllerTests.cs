using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Service;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.Tests
{
    [TestClass]
    public class ProductControllerTests
    {
        private ProductCrudController _controller;
        private GameCrudController _gameController;
        private Mock<IService<Product>> _productServiceMock;
        private Mock<IService<Game>> _gameServiceMock;

        public ProductControllerTests()
        {
            _productServiceMock = new Mock<IService<Product>>();
            _gameServiceMock = new Mock<IService<Game>>();
            _controller = new ProductCrudController(_productServiceMock.Object);
            _gameController = new GameCrudController(_gameServiceMock.Object);
        }

        //Un GET Product avec un id � 0 doit renvoyer une erreur
        [TestMethod]
        public void Get_With0_ThenGetFromRepoIsNotCalled()
        {
            _controller.Get(0);
            _productServiceMock.Verify(x => x.Get(0), Times.Never());
        }

        //Un GET Product avec un id � 1 doit renvoyer un r�sultat
        [TestMethod]
        public void Get_With1_ThenGetFromRepoIsCalledWith1()
        {
            _controller.Get(1);
            _productServiceMock.Verify(x => x.Get(1), Times.Once());
        }

        //La cr�ation d'un Product sans param�tres doit �chouer
        [TestMethod]
        public void Add_WithNoParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new Product());
            _productServiceMock.Verify(x => x.Add(It.IsAny<Product>()), Times.Never());
        }

        //La cr�ation d'un Product avec des param�tres manquants doit �chouer
        [TestMethod]
        public void Add_WithMissingParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new Product()
            {
                Name = "test@gmail.com",
            });

            _productServiceMock.Verify(x => x.Add(It.IsAny<Product>()), Times.Never());
        }

        //La cr�ation d'un Product avec tous les param�tres n�cessaires doit r�ussir
        [TestMethod]
        public void Add_WithAllParameters_ThenAddIsCalled()
        {
            _controller.Create(new Product()
            {
                Name = "test@gmail.com",
                Game = new Game() { Id = 1 },
                PictureUrl = "picture.url",
                Description = "description"
            });

            _productServiceMock.Verify(x => x.Add(It.IsAny<Product>()), Times.Once());
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID inexistant doit �chouer
        [TestMethod]
        public void DeleteWithUnknownId_ThenDeleteIsNotOk()
        {
            _productServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(false);

            var deletedProduct = _controller.Delete(55);

            Assert.IsInstanceOfType(deletedProduct, typeof(NotFoundResult));
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID existant doit r�ussir
        [TestMethod]
        public void DeleteWithExistingId_ThenDeleteIsOk()
        {
            _productServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new Product());
            _productServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(true);

            var deletedProduct = _controller.Delete(1);

            Assert.IsInstanceOfType(deletedProduct, typeof(OkResult));
        }
    }
}