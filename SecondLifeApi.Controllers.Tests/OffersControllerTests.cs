﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Service.Services.IServices;
using SecondLifeApi.Controllers.metier;
using SecondLifeModel.Entities;
using System.Collections.Generic;

namespace SecondLifeApi.Controllers.Tests
{
    [TestClass]
    public class OffersControllerTests
    {
        private OffersController _controller;
        private Mock<HttpRequest> _mockRequest;
        private Mock<IAuthService> _authServiceMock;
        private Mock<IOfferService> _offerServiceMock;
        private Mock<IProposalService> _proposalServiceMock;

        public OffersControllerTests()
        {
            _mockRequest = new Mock<HttpRequest>();
            _offerServiceMock = new Mock<IOfferService>();
            _authServiceMock = new Mock<IAuthService>();
            _proposalServiceMock = new Mock<IProposalService>();
            _controller = new OffersController(_offerServiceMock.Object, _proposalServiceMock.Object, _authServiceMock.Object);
        }

        public User getUser() { return new User() { Id = 1, Mail = "anthony@test.com", Username = "anthony", Password = "password", PhoneNumber = "0606060606", StreetNumber = 12, Street = "rue de Paris", City = "STRASBOURG", PostalCode = "67000" }; }

        public Game getGame() { return new Game() { Id = 1, Name = "Jeu", Score = 5, Description = "Description", PictureUrl = "http://www.google.fr" }; }

        public List<Product> getProducts() { return new List<Product>() { new Product() { Description = "description", Name = "", PictureUrl = "", Game = getGame() } }; }

        public Offer getOffer() { return new Offer() { Title = "Vend 1 jeu PS2 bon état", Description = "Lorem ipsum", BasePrice = 4, Products = getProducts(), Seller = getUser() }; }

        public List<Offer> getOffers() { return new List<Offer>() { getOffer() }; }
        
        [TestMethod]
        public void CallCreateAnOfferWithUnknownUser_ThenReturnsUnauthorized()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns((User)null);

            var offer = _controller.CreateAnOffer(getOffer());

            Assert.IsInstanceOfType(offer, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public void CallCreateAnOfferWithImproperOffer_ThenReturnsConflict()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns(getUser());
            _offerServiceMock.Setup(x => x.CreateAnOffer(It.IsAny<Offer>(), It.IsAny<User>())).Returns((Offer)null);

            var offer = _controller.CreateAnOffer(getOffer());

            Assert.IsInstanceOfType(offer, typeof(ConflictObjectResult));
        }

        [TestMethod]
        public void CallCreateAnOfferWithOffer_ThenReturnsOk()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns(getUser());
            _offerServiceMock.Setup(x => x.CreateAnOffer(It.IsAny<Offer>(), It.IsAny<User>())).Returns(getOffer());

            var offer = _controller.CreateAnOffer(getOffer());

            Assert.IsInstanceOfType(offer, typeof(OkObjectResult));
        }

        [TestMethod]
        public void CallGetOfferByUnknownId_ThenReturnsForbid()
        {
            _offerServiceMock.Setup(x => x.GetOfferById(It.IsAny<int>())).Returns((Offer)null);

            var offer = _controller.GetOfferById(5);

            Assert.IsInstanceOfType(offer, typeof(ForbidResult));
        }

        [TestMethod]
        public void CallGetOfferByKnownId_ThenReturnsOk()
        {
            _offerServiceMock.Setup(x => x.GetOfferById(It.IsAny<int>())).Returns(getOffer());

            var offer = _controller.GetOfferById(1);

            Assert.IsInstanceOfType(offer, typeof(OkObjectResult));
        }

        [TestMethod]
        public void CallGetMyOffersBeingSold_WithUnknownUser_ThenReturnsUnauthorized()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns((User)null);

            var offer = _controller.GetMyOffersBeingSold();

            Assert.IsInstanceOfType(offer, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public void CallGetMyOffersBeingPurchased_WithUnknownUser_ThenReturnsUnauthorized()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns((User)null);

            var offer = _controller.GetMyOffersBeingPurchased();

            Assert.IsInstanceOfType(offer, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public void CallGetMyOffersBeingSold_WhichIsEmpty_ThenReturnsNoContent()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns(getUser());
            _offerServiceMock.Setup(x => x.GetMyOffersBeingSold(It.IsAny<User>())).Returns((List<Offer>)null);

            var offer = _controller.GetMyOffersBeingSold();

            Assert.IsInstanceOfType(offer, typeof(NoContentResult));
        }

        [TestMethod]
        public void CallGetMyOffersBeingSold_ThenReturnsOkObjectResult()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns(getUser());
            _offerServiceMock.Setup(x => x.GetMyOffersBeingSold(It.IsAny<User>())).Returns(getOffers());

            var offer = _controller.GetMyOffersBeingSold();

            Assert.IsInstanceOfType(offer, typeof(OkObjectResult));
        }

        [TestMethod]
        public void CallGetMyOffersBeingPurchased_WhichIsEmpty_ThenReturnsNoContent()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns(getUser());
            _offerServiceMock.Setup(x => x.GetMyOffersBeingPurchased(It.IsAny<User>())).Returns((List<Offer>)null);

            var offer = _controller.GetMyOffersBeingPurchased();

            Assert.IsInstanceOfType(offer, typeof(NoContentResult));
        }

        [TestMethod]
        public void CallGetMyOffersBeingPurchased_ThenReturnsOkObjectResult()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns(getUser());
            _offerServiceMock.Setup(x => x.GetMyOffersBeingPurchased(It.IsAny<User>())).Returns(getOffers());

            var offer = _controller.GetMyOffersBeingPurchased();

            Assert.IsInstanceOfType(offer, typeof(OkObjectResult));
        }
    }
}