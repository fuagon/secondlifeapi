using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Service;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.Tests
{
    [TestClass]
    public class GameControllerTests
    {
        private GameCrudController _controller;
        private Mock<IService<Game>> _gameServiceMock;

        public GameControllerTests()
        {
            _gameServiceMock = new Mock<IService<Game>>();
            _controller = new GameCrudController(_gameServiceMock.Object);
        }

        //Un GET Game avec un id � 0 doit renvoyer une erreur
        [TestMethod]
        public void Get_With0_ThenGetFromRepoIsNotCalled()
        {
            _controller.Get(0);
            _gameServiceMock.Verify(x => x.Get(0), Times.Never());
        }

        //Un GET Game avec un id � 1 doit renvoyer un r�sultat
        [TestMethod]
        public void Get_With1_ThenGetFromRepoIsCalledWith1()
        {
            _controller.Get(1);
            _gameServiceMock.Verify(x => x.Get(1), Times.Once());
        }

        //La cr�ation d'un Game sans param�tres doit �chouer
        [TestMethod]
        public void Add_WithNoParameters_ThenAddIsNotCalled()
        {
            _gameServiceMock.Setup(x => x.Add(It.IsAny<Game>())).Returns((Game)null);

            var gameCreated = _controller.Create(new Game());

            Assert.IsInstanceOfType(gameCreated, typeof(BadRequestResult));
        }

        //La cr�ation d'un Game avec des param�tres manquants doit �chouer
        [TestMethod]
        public void Add_WithMissingParameters_ThenAddIsNotCalled()
        {
            _gameServiceMock.Setup(x => x.Add(It.IsAny<Game>())).Returns((Game)null);

            var gameCreated = _controller.Create(new Game()
            {
                Name = "Jeu",
                Description = "Description de jeu",  
            });

        }

        //La cr�ation d'un Game avec tous les param�tres n�cessaires doit r�ussir
        [TestMethod]
        public void Add_WithAllParameters_ThenAddIsCalled()
        {
            var game = new Game()
            {
                Name = "Jeu",
                Description = "Description de jeu",
                PictureUrl = "https://url.com",
                Score = 5
            };

            _gameServiceMock.Setup(x => x.Add(It.IsAny<Game>())).Returns(game);

            var gameCreated = _controller.Create(game);

            Assert.IsInstanceOfType(gameCreated, typeof(OkObjectResult));
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID inexistant doit �chouer
        [TestMethod]
        public void DeleteWithUnknownId_ThenDeleteIsNotOk()
        {
            _gameServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(false);

            var deletedGame = _controller.Delete(55);

            Assert.IsInstanceOfType(deletedGame, typeof(NotFoundResult));
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID existant doit r�ussir
        [TestMethod]
        public void DeleteWithExistingId_ThenDeleteIsOk()
        {
            _gameServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new Game());
            _gameServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(true);

            var deletedGame = _controller.Delete(1);

            Assert.IsInstanceOfType(deletedGame, typeof(OkResult));
        }
    }
}