﻿using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.IServices
{
    public interface IOpinionService
    {
        public Opinion GetById(int id);
    }
}