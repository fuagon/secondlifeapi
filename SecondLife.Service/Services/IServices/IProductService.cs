﻿using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.IServices
{
    public interface IProductService
    {
        public Product GetProduct(int id);
    }
}