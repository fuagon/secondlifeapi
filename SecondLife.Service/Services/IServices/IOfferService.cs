﻿using SecondLifeModel.Entities;
using System.Collections.Generic;

namespace SecondLife.Service.Services.IServices
{
    public interface IOfferService
    {
        public Offer CreateAnOffer(Offer offer, User u);
        public Offer GetOfferById(int id);
        public List<Offer> GetMyOffersBeingSold(User u);
        public List<Offer> GetMyOffersBeingPurchased(User u);
        public List<Offer> GetAllOffers();
        public Proposal DeclineProposal(User user, int id);
        public Proposal AcceptProposal(User user, int id, string message);
        public Proposal MakeAProposal(User u, Proposal proposal);
        public List<Offer> GetAllOffersWithoutBuyer();
        public bool CheckIfActiveProposal(User u, Offer o);
        public Opinion AddAnOpinion(Opinion opinion, Offer offer, User user);
        public void AddBuyerToAnOffer(Offer offer, User user);
        public bool CheckIfOpinionExists(Offer offer);
        public Product CreateProduct(Product product);
        public bool DeleteMyOffer(User user, int id);
    }
}