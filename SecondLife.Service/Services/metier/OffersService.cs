﻿using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;
using System.Collections.Generic;
using System.Linq;

namespace SecondLife.Service.Services
{
    public class OffersService : IOfferService
    {
        private IService<Offer> _offersService;
        private IService<Product> _productService;
        private IProposalService _proposalService;
        private IService<Game> _gameService;
        private IService<Opinion> _opinionService;
        private IAuthService _authService;
        private IValidator<Offer>_offersValidator;
        private IValidator<Product> _productValidator;
        private IValidator<Proposal> _proposalValidator;

        public OffersService(IService<Offer> offersService, IService<Product> productService, IProposalService proposalService, IService<Game> gameService, IAuthService authService, IValidator<Offer> offersValidator, IValidator<Product> productValidator, IValidator<Proposal> proposalValidator, IService<Opinion> opinionService)
        {
            _offersService = offersService;
            _productService = productService;
            _proposalService = proposalService;
            _gameService = gameService;
            _authService = authService;
            _opinionService = opinionService;
            _offersValidator = offersValidator;
            _productValidator = productValidator;
            _proposalValidator = proposalValidator;
        }

        public Offer CreateAnOffer(Offer offer, User seller)
        {
            if (offer.Products != null)
            {
                bool productsValidated = true;

                var listProduct = offer.Products.Select(p => {
                    if (productsValidated)
                    {
                        p.Game = _gameService.Get(p.Game.Id);
                        if (_productValidator.CanAdd(p))
                            return _productService.Add(p);
                        else { 
                            productsValidated = false;
                            return null;
                        }
                    }
                    else
                        return null;
                }).ToList();

                if (productsValidated != false)
                {
                    offer.Products = listProduct;
                    offer.Seller = seller;

                    if (_offersValidator.CanAdd(offer))
                        return _offersService.Add(offer);
                }
            }

            return null;
        }

        public bool DeleteMyOffer(User user, int id)
        {
            var offer = _offersService.Get(id);

            if (offer != null) {
                if (_offersValidator.CanDelete(id)) {
                    if (user.Id == offer.Seller.Id)
                        return _offersService.Delete(offer.Id);
                }
            }

            return false;
        }

        public List<Offer> GetAllOffers()
        {
            return _offersService.All();
        }

        public List<Offer> GetAllOffersWithoutBuyer()
        {            
            return _offersService.All(o => o.Buyer == null);
        }

        // Liste des offres mises en vente par l'utilisateur
        public List<Offer> GetMyOffersBeingSold(User u)
        {
            return _offersService.All(o => o.Seller.Id == u.Id);
        }

        // Liste des offres sur lesquelles l'utilisateur a fait une Proposition
        public List<Offer> GetMyOffersBeingPurchased(User u)
        {
            return _proposalService.GetAllUserProposals(u).Select(p => {
                p.Offer = _offersService.Get(p.Offer.Id);
                return p.Offer;
            }).ToList();
        }

        public Product CreateProduct(Product product)
        {
            if (_productValidator.CanAdd(product))
                return _productService.Add(product);

            return null;
        }

        public Offer GetOfferById(int id)
        {
            return _offersService.Get(id);
        }

        public Proposal MakeAProposal(User requestingUser, Proposal proposal)
        {
            if (proposal.Offer != null)
            {
                var offer = GetOfferById(proposal.Offer.Id);

                if (offer != null && offer.Buyer == null)
                {
                    if (offer.Seller.Id != requestingUser.Id)
                    {
                        if (!CheckIfActiveProposal(requestingUser, offer))
                        {
                            var allProductsValid = false;

                            //On passe en revue tous les produits
                            foreach (var product in proposal.ProposedProducts)
                            {
                                if (_productValidator.CanAdd(product))
                                    allProductsValid = true;
                                else {
                                    allProductsValid = false;
                                    break;
                                }
                            }

                            //S'ils ont tous été validés par le validator, on les ajoute
                            if (allProductsValid)
                            {
                                foreach (var product in proposal.ProposedProducts)
                                    _productService.Add(product);

                                proposal.RequestingUser = requestingUser;
                                proposal.ProposalState = ProposalState.Waiting;
                                proposal.Offer = offer;

                                if (_proposalValidator.CanAdd(proposal))
                                    return _proposalService.Create(proposal);
                            }   
                        }
                    }
                }
            }
         
            return null;
        }

        public bool CheckIfActiveProposal(User u, Offer o)
        {
            bool res = false;

            _proposalService.GetOffersProposals(o).ForEach(p =>
            {
                if (p.RequestingUser.Id == u.Id && p.ProposalState == ProposalState.Waiting)
                {
                    res = true;
                }
            });

            return res;
        }

        public Proposal DeclineProposal(User seller, int proposalId)
        {
            var proposal = _proposalService.GetById(proposalId);

            if (proposal != null && proposal.Offer != null)
            {
                var offer = GetOfferById(proposal.Offer.Id);

                if (offer != null) {
                    if (offer.Seller.Id == seller.Id) {
                        if (_proposalValidator.CanEdit(proposal))
                            return _proposalService.DeclineProposal(proposal);
                    }
                }
            }

            return null;
        }

        public Proposal AcceptProposal(User u, int incomingProposalId, string message)
        {
            var proposal = _proposalService.GetById(incomingProposalId);

            if (proposal != null)
            {
                var proposalList = _proposalService.GetOffersProposals(proposal.Offer);

                if (proposal.Offer.Seller.Id == u.Id)
                    if (_proposalValidator.CanEdit(proposal))
                        return _proposalService.AcceptProposal(proposalList, incomingProposalId, message);

            }

            return null;
        }

        public bool CheckIfOpinionExists(Offer o)
        {
            var offer = GetOfferById(o.Id);

            return offer.Opinion != null;
        }

        public Opinion AddAnOpinion(Opinion opinion, Offer offer, User user)
        {
            //Seul la personne ayant remporté l'offre peut donner son opinion sur cette transaction
            if (offer.Buyer != user)
                return null;

            opinion.RequestingUser = user;
            opinion.TargetedUser = offer.Seller;

            var opinionCreated = _opinionService.Add(opinion);

            offer.Opinion = opinion;

            _offersService.Update(offer);

            return opinionCreated;
        }

        public void AddBuyerToAnOffer(Offer offer, User user)
        {
            offer.Buyer = user;
            _offersService.Update(offer);
        }
    }
}