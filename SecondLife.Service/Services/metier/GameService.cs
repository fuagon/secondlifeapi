﻿using SecondLifeModel.Entities;
using System.Collections.Generic;

namespace SecondLife.Service.Services.metier
{
    public class GameService
    {
        private IService<Game> _service;

        public GameService(IService<Game> service)
        {
            _service = service;
        }

        public List<Game> GetAllGame()
        {
            return _service.All();
        }
    }
}
