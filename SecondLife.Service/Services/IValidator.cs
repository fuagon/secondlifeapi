﻿using System;
using System.Collections.Generic;
using System.Text;


namespace SecondLife.Service
{
    public interface IValidator<T>
    {
        bool CanAdd(T canAdd);
        bool CanDelete(int canDelete);
        bool CanRead(int canRead);
        bool CanEdit(T canEdit);
    }
}
