﻿using SecondLife.Repositories;
using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.Validator
{
    public class ProductValidator : IValidator<Product>
    {
        private IRepositorie<Product> _repo;

        public bool CanAdd(Product product)
        {
            if (product.Name != null && product.Description != null && product.PictureUrl != null && product.Game != null)
                return true;

            return false;
        }

        public bool CanDelete(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }

        public bool CanEdit(Product product)
        {
            if (_repo.Exists(_repo.Get(product.Id)))
                return true;

            return false;
        }

        public bool CanRead(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }
    }
}