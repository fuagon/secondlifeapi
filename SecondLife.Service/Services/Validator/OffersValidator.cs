﻿using SecondLife.Repositories;
using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.Validator
{
    public class OffersValidator : IValidator<Offer>
    {
        private IRepositorie<Offer> _repo;

        public bool CanAdd(Offer offer)
        {
            if (offer.Title != null && offer.BasePrice > 0 && offer.Seller != null && offer.Description != null)
                return true;

            return false;
        }

        public bool CanDelete(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }

        public bool CanEdit(Offer offer)
        {
            if (_repo.Exists(_repo.Get(offer.Id)))
                return true;

            return false;
        }

        public bool CanRead(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }
    }
}
