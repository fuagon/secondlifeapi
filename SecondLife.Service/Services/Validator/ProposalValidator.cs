﻿using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.Validator
{
    public class ProposalValidator : IValidator<Proposal>
    {
        public bool CanAdd(Proposal canAdd)
        {
            return true;
        }

        public bool CanDelete(int canDelete)
        {
            return true;
        }

        public bool CanEdit(Proposal canEdit)
        {
            return true;
        }

        public bool CanMake(User u, int idOffer, Proposal canAdd)
        {
            return true;
        }

        public bool CanRead(int canRead)
        {
            return true;
        }
    }
}
