﻿using SecondLifeModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondLife.Repositories
{
    public interface IOfferRepository : IRepositorie<Offer> { }
   
}
