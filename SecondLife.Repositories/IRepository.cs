using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SecondLife.Repositories
{
    public interface IRepositorie<T> where T : class
    {
        public List<T> All(Expression<Func<T, bool>> condition = null);
        T Get(int repositorieId);
        T One();
        T Add(T repositorieUpdate);
        void Update(T repositorieUpdate);
        bool Delete(int repositoryId);
        bool Exists(T repositorieExists);
        T Find(Expression<Func<T, bool>> condition);   
    }
}