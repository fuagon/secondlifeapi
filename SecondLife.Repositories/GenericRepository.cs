using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using SecondLifeModel;

namespace SecondLife.Repositories
{
    public class GenericRepository<T> : IRepositorie<T> where T : class
    {
        private readonly SecondLifeDbContext _context;
        protected IQueryable<T> _contextWithIncludes;
        protected virtual List<string> _includes { get; } = new List<string>();

        public GenericRepository(SecondLifeDbContext context)
        {
            _context = context;
            _contextWithIncludes = _context.Set<T>().AsQueryable();
            foreach(var item in _includes)
            {
                Console.WriteLine(item.ToString());
                _contextWithIncludes = _contextWithIncludes.Include(item);
            }
        }

        public virtual List<T> All(Expression<Func<T, bool>> condition = null)
        {
            condition ??= x => true;
            var a = _contextWithIncludes.Where(condition).ToList();
            return a;
        }

        public virtual T Get(int repositorieId)
        {
            return _context.Set<T>().Find(repositorieId);
        }

        public T One()
        {
            return _context.Set<T>().FirstOrDefault();
        }

        public T Add(T repositorieAdd)
        {
            _context.Entry(repositorieAdd).State = EntityState.Detached;
            _context.Add(repositorieAdd);
            _context.SaveChanges();
            return repositorieAdd;
        }

        public void Update(T objectToUpdate)
        {
            // 
           
            _context.Attach(objectToUpdate);
            _context.Entry(objectToUpdate).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public bool Delete(int repositoryId)
        {
            var obj = Get(repositoryId);
            if (obj == null)
                return false;

            _context.Set<T>().Remove(obj);
            _context.SaveChanges();
            return Get(repositoryId) == null;
        }

        public bool Exists(T repositorieExists)
        {
            return _context.Set<T>().FirstOrDefault(x => x.Equals(repositorieExists)) != null;       
        }
        public T Find(Expression<Func<T, bool>> condition)
        {
            return _context.Set<T>().FirstOrDefault(condition);
        }
    }
}