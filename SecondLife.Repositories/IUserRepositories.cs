﻿using SecondLifeModel.Entities;
using System;
using System.Linq.Expressions;

namespace SecondLife.Repositories
{
    public interface IUserRepositories : IRepositorie<User> 
    {
        new bool Exists(User userExist);
        new User Find(Expression<Func<User, bool>> condition);
    }
}