﻿using SecondLifeModel.Entities;

namespace SecondLife.Repositories
{
    public interface IGameRepositories : IRepositorie<Game> { }
}