﻿using SecondLifeModel;
using SecondLifeModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SecondLife.Repositories.Repositories
{
    public class OfferRepositories : GenericRepository<Offer>, IRepositorie<Offer>
    {
        private readonly SecondLifeDbContext _context;
        protected override List<string> _includes => new List<String> { nameof(Offer.Products),
            nameof(Offer.Seller), nameof(Offer.Buyer), nameof(Offer.Proposals), nameof(Offer.Opinion), "Products.Game", "Proposals.ProposedProducts.Game"};
        public OfferRepositories(SecondLifeDbContext context) : base(context)
        {

        }

        public override Offer Get(int repositorieId)
        {
            return _contextWithIncludes.FirstOrDefault(x => x.Id == repositorieId);
        }
    }
}