﻿using Microsoft.EntityFrameworkCore;
using SecondLifeModel;
using SecondLifeModel.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace SecondLife.Repositories.Repositories
{
    public class UserRepositories : GenericRepository<User>, IUserRepositories
    {
        private readonly SecondLifeDbContext _context;

        public UserRepositories(SecondLifeDbContext context) : base(context)
        {
            _context = context;
        }

        public new bool Exists(User user)
        {
            User dbUser = null;
            if (user != null)
            {
                dbUser = _context.Users.FirstOrDefault(x => x.Username == user.Username || x.Mail == user.Mail);
            }
            
            return dbUser != null ;
        }
        
        public new void Update(User updateUser)
        {
            var local = _context.Set<User>()
               .Local
               .FirstOrDefault(entry => entry.Id.Equals(updateUser.Id));

            if (local != null)
            {
                _context.Entry(local).State = EntityState.Detached;
            }
            _context.Attach(updateUser);
            _context.Entry(updateUser).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public new User Find(Expression<Func<User, bool>> condition)
        {
            return _context.Set<User>().FirstOrDefault(condition);
        }
    }
}