using SecondLifeModel.Entities;
using System;
using System.Linq;
using SecondLifeModel;
using System.Linq.Expressions;

namespace SecondLife.Repositories.Repositories
{
    public class OpinionRepositories : GenericRepository<Opinion>, IOpinionRepositories
    {
        private readonly SecondLifeDbContext _context;

        public OpinionRepositories(SecondLifeDbContext context) : base(context)
        {
            _context = context;
        }

        public new Opinion One()
        {
            return _context.Opinions.FirstOrDefault();
        }

        public new Opinion Add(Opinion opinion)
        {
            _context.Add(opinion);
            _context.SaveChanges();
            return opinion;
        }

        public new void Update(Opinion updateOpinion)
        {
            _context.Attach(updateOpinion);
        }

        public new bool Exists(Opinion opinionExists)
        {
            var dbProduct = _context.Opinions.FirstOrDefault(x => x.Id == opinionExists.Id);
            return opinionExists != null;
            
        }

        public new Opinion Get(int opinionId)
        {
            return _context.Opinions.Find(opinionId);
        }

        public new bool Delete(int opinionDelete)
        {
            var obj = Get(opinionDelete);
            if (obj == null)
                return false;
            
            
            _context.Opinions.Remove(obj);
            _context.SaveChanges();
            return Get(opinionDelete) == null;
        }

        public new Opinion Find(Expression<Func<Opinion, bool>> condition)
        {
            throw new NotImplementedException();
        }
    }
}