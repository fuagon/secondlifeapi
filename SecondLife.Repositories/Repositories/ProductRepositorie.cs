﻿using SecondLifeModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using SecondLifeModel;
using Microsoft.EntityFrameworkCore;

namespace SecondLife.Repositories.Repositories
{
    public class ProductRepositorie : GenericRepository<Product>, IProductRepositorie
    {
        private readonly SecondLifeDbContext _context;

        protected override List<string> _includes => new List<String> { nameof(Product.Game) };

        public ProductRepositorie(SecondLifeDbContext context) : base(context) 
        {
            _context = context;
        }

        public override Product Get(int repositorieId)
        {
            return _contextWithIncludes.FirstOrDefault(x => x.Id == repositorieId);
        }

        public new Product Add(Product product)
        {
            _context.Entry<Game>(product.Game).State = EntityState.Detached;
            _context.Entry<Product>(product).State = EntityState.Added;
            _context.Products.Add(product);
            _context.SaveChanges();

            return product;
        }
    }
}
