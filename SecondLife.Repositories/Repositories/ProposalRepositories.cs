using System;
using System.Collections.Generic;
using System.Linq;
using SecondLifeModel;
using SecondLifeModel.Entities;

namespace SecondLife.Repositories.Repositories
{
    public class ProposalRepositories : GenericRepository<Proposal>, IRepositorie<Proposal>
    {
        private readonly SecondLifeDbContext _context;
        protected override List<string> _includes => new List<String>
        { nameof(Proposal.Offer), nameof(Proposal.ProposedProducts), "Offer.Products", "Offer.Products.Game", nameof(Proposal.RequestingUser), "ProposedProducts.Game"};

        public ProposalRepositories(SecondLifeDbContext context) : base (context)
        {
            _context = context;
        }

        public override Proposal Get(int proposalId)
        {
            return _contextWithIncludes.FirstOrDefault(x => x.Id == proposalId);
        }
    }
}