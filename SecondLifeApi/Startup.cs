using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SecondLife.Repositories;
using SecondLife.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SecondLife.Service.Services;
using SecondLife.Repositories.Repositories;
using SecondLife.Service.Services.IServices;
using SecondLife.Service.Services.Validator;
using SecondLifeModel.Entities;
using Microsoft.AspNetCore.Identity;
using SecondLife.Services.Tests;
using SecondLifeApi.Controllers;
using SecondLife.Service.Services.metier;

namespace SecondLifeApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddControllers();
            services
                .AddControllersWithViews()
                .AddNewtonsoftJson();
            InjectRepository(services);
            InjectService(services);
            var dbconnect = Configuration.GetConnectionString("Dbcontext");
            services.AddDbContextPool<SecondLifeModel.SecondLifeDbContext>(x => x.UseMySql(dbconnect));
            
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
        }
        private static void InjectRepository(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepositorie<>), typeof(GenericRepository<>));
            services.AddScoped<IRepositorie<Offer>, OfferRepositories>();
            services.AddScoped<IRepositorie<User>, UserRepositories>();
            services.AddScoped<IRepositorie<Product>, ProductRepositorie>();
            services.AddScoped<IRepositorie<Proposal>, ProposalRepositories>();
            services.AddScoped<IUserRepositories, UserRepositories>();
        }
        private static void InjectService(IServiceCollection services)
        {
            services.AddScoped(typeof(IService<>), typeof(GenerericService<>));
            //services.AddScoped(typeof(User<>), typeof(GenericValidator<>));
            services.AddScoped(typeof(IValidator<>), typeof(Validator<>));
            //services.AddScoped(typeof(IValidator<User>), typeof(UserValidator<>));


            //services.AddScoped(typeof(IValidator<>), typeof(GenericValidator<>));

            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IOfferService, OffersService>();
            services.AddScoped<IProposalService, ProposalService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IOpinionService, OpinionService>();

            //Service M�tier
            services.AddScoped<AuthService>();
            services.AddScoped<OffersService>(); 
            services.AddScoped<GameService>();
            services.AddScoped<ProposalService>();


            //Validator
            services.AddScoped<UserValidator>();
            services.AddScoped<OffersValidator>();
            services.AddScoped<ProductValidator>();
            services.AddScoped<ProposalValidator>();
            services.AddScoped<GameValidator>();

        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                
            });
        }
    }
}
