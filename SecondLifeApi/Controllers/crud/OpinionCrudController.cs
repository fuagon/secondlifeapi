﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpinionCrudController : ControllerBase
    {
        private IService<Opinion> _opinion;

        public OpinionCrudController(IService<Opinion> opinion)
        {
            _opinion = opinion;
        }

        [HttpGet("{id}")]
        public ActionResult<Opinion> Get(int id)
        {
            if (id != 0)
                return _opinion.Get(id);

            return NoContent();
        }

        [HttpGet]
        public ActionResult<List<Opinion>> List()
        {
            var res = _opinion.All();

            if (res.Count == 0)
                return NoContent();

            return res;
        }

        [HttpPost]
        public ActionResult<Opinion> Create(Opinion opinion)
        {
            Opinion res = null;

            if (opinion.RequestingUser != null && 
                opinion.TargetedUser != null && 
                opinion.Comment != null)
                res = _opinion.Add(opinion);

            if (res == null)
                return BadRequest();

            return res;
        }

        [HttpPatch("{id}")]
        public ActionResult<Opinion> Patch(int id, [FromBody] JsonPatchDocument<Opinion> document) // [FromBody] forcer l’API Web à lire un type simple à partir du corps de la requête
        {
            var updatedOpinion = _opinion.Patch(id, document);

            return Ok(updatedOpinion);
        }

        [HttpPut]
        public ActionResult<Opinion> Update(Opinion opinion)
        {
            _opinion.Update(opinion);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (id != 0)
                if (_opinion.Get(id) != null)
                    if (_opinion.Delete(id))
                        return Ok();

            return NotFound();
        }
    }
}