﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service;
using SecondLifeModel.Entities;
using System.Collections.Generic;

namespace SecondLifeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfferCrudController : ControllerBase
    {
        private IService<Offer> _offer;

        public OfferCrudController(IService<Offer> offer)
        {
            _offer = offer;
        }

        [HttpGet("{id}")]
        public ActionResult<Offer> Get(int id)
        {
            if (id != 0)
                return _offer.Get(id);

            return NoContent();
        }

        [HttpGet]
        public ActionResult<List<Offer>> List()
        {
            var res = _offer.All();

            if (res.Count == 0)
                return NoContent();

            return res;
        }

        [HttpPost]
        public ActionResult<Offer> Create(Offer offer)
        {
            Offer res = null;

            if (offer.Title != null && 
                offer.BasePrice > 0 && 
                offer.Seller != null && 
                offer.Description != null)
                res = _offer.Add(offer);

            if (res == null)
                return BadRequest();

            return res;
        }

        [HttpPatch("{id}")]
        public ActionResult<Offer> Patch(int id, [FromBody] JsonPatchDocument<Offer> document) // [FromBody] forcer l’API Web à lire un type simple à partir du corps de la requête
        {
            var updatedOffer = _offer.Patch(id, document);

            return Ok(updatedOffer);
        }

        [HttpPut]
        public ActionResult<Offer> Update(Offer offer)
        {
            _offer.Update(offer);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (id != 0)
                if (_offer.Get(id) != null)
                    if (_offer.Delete(id))
                        return Ok();

            return NotFound();
        }
    }
}