﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondLifeApi.Controllers
{
    [Route("api/[controller]")]
    public class PropositionController : ControllerBase
    {
        private IService<PropositionController> _proposition;

        public PropositionController(IService<PropositionController> proposition)
        {
            _proposition = proposition;
        }

        [HttpGet("{id}")]
        public ActionResult<PropositionController> Get(int id)
        {
            if (id != 0)
                return _proposition.Get(id);

            return NoContent();
        }

        [HttpGet]
        public ActionResult<List<PropositionController>> List()
        {
            var res = _proposition.All();

            if (res.Count == 0)
                return NoContent();

            return res;
        }

        [HttpPost]
        public ActionResult<PropositionController> Create(PropositionController proposition)
        {
            var res = _proposition.Add(proposition);

            if (res == null)
                return BadRequest();

            return res;
        }

        [HttpPatch("{id}")]
        public ActionResult<PropositionController> Patch(int id, [FromBody] JsonPatchDocument<PropositionController> document) // [FromBody] forcer l’API Web à lire un type simple à partir du corps de la requête
        {
            var updatedProposition = _proposition.Patch(id, document);

            return Ok(updatedProposition);
        }

        [HttpPut]
        public ActionResult<PropositionController> Update(PropositionController proposition)
        {
            _proposition.Update(proposition);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (id != 0)
                if (_proposition.Get(id) != null)
                    if (_proposition.Delete(id))
                        return Ok();

            return NotFound();
        }
    }
}