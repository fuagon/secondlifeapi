﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondLifeApi.Controllers
{
    [Route("api/[controller]")]
    public class PropositionCrudController : ControllerBase
    {
        private IService<PropositionCrudController> _service;

        public PropositionCrudController(IService<PropositionCrudController> service)
        {
            _service = service;
        }

        [HttpPatch("{id}")]
        public ActionResult<PropositionCrudController> Patch(int id, JsonPatchDocument<PropositionCrudController> document)
        {
            var r = _service.Patch(id, document);
            
            return Ok(r);
        }

        [HttpPost]
        public ActionResult<PropositionCrudController> Post(PropositionCrudController proposition)
        {
            var res = _service.Add(proposition);
            if(res == null) 
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        public ActionResult<PropositionCrudController> Get(int propositionId)
        {
            return _service.Get(propositionId);
        }
    }
}
