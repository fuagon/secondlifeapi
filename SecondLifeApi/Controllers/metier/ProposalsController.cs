﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.metier
{
    [ApiController, Route("api/[controller]"), Authorize]
    public class ProposalsController : ControllerBase
    {
        private IProposalService _service;
        private IAuthService _authService;

        public ProposalsController(IProposalService service, IAuthService authService)
        {
            _service = service;
            _authService = authService;
        }

        [HttpGet("{id}")]
        public IActionResult GetProposal(int id)
        {
            Proposal proposal = _service.GetById(id);

            if (proposal != null)
                return Ok(proposal);

            return NotFound();
        }
    }
}