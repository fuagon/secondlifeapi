using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondLife.Repositories;
using Moq;
using SecondLife.Service;
using SecondLifeModel.Entities;
using SecondLife.Service.Services.IServices;
using SecondLife.Service.Services.metier;
using System.Collections.Generic;

namespace SecondLife.Services.Tests
{
    [TestClass]
    public class ProposalServiceTests
    {
        private ProposalService _service;
        private Mock<IRepositorie<User>> _repo;

        private Mock<IValidator<User>> _validator;
        private Mock<IValidator<Product>> _productValidator;
        private Mock<IValidator<Offer>> _offerValidator;
        private Mock<IValidator<Proposal>> _proposalValidator;

        private Mock<IService<User>> _userService;
        private Mock<IService<Product>> _productService;
        private Mock<IService<Game>> _gameService;
        private Mock<IService<Opinion>> _opinionService;

        private Mock<IService<Proposal>> _proposalService;
        private Mock<IAuthService> _authService;
        private Mock<IService<Offer>> _offerService;

        public User getBuyer() { return new User() { Id = 1, Mail = "user1@test.com", Username = "user1", Password = "password", PhoneNumber = "0606060606", StreetNumber = 12, Street = "rue de Paris", City = "STRASBOURG", PostalCode = "67000" }; }
        public User getSeller() { return new User() { Id = 2, Mail = "user2@test.com", Username = "user2", Password = "password", PhoneNumber = "0707070707", StreetNumber = 13, Street = "rue de Strasbourg", City = "PARIS", PostalCode = "78000" }; }
        public Game getGame() { return new Game() { Id = 1, Name = "Jeu", Score = 5, Description = "Description", PictureUrl = "http://www.google.fr" }; }
        public Product getProduct() { return new Product() { Id = 1, Name = "Biomutant", PictureUrl = "url", Game = getGame(), Description = "Le jeu Biomutant" }; }
        public List<Product> getProducts() { return new List<Product>() { new Product() { Description = "description", Name = "", PictureUrl = "", Game = getGame() } }; }
        public Offer getOffer() { return new Offer() { Title = "Vend 1 jeu PS2 bon �tat", Description = "Lorem ipsum", BasePrice = 4, Products = getProducts(), Seller = getSeller() }; }
        public Proposal getProposal() { return new Proposal() { Id = 1, Offer = getOffer(), ProposalState = ProposalState.Waiting, ProposedPrice = 100, RequestingUser = getBuyer(), ProposedProducts = new List<Product>() { getProduct() } }; }

        public ProposalServiceTests()
        {
            _offerService = new Mock<IService<Offer>>();
            _productService = new Mock<IService<Product>>(); ;

            _gameService = new Mock<IService<Game>>();
            _opinionService = new Mock<IService<Opinion>>();
            _productValidator = new Mock<IValidator<Product>>();
            _offerValidator = new Mock<IValidator<Offer>>();
            _validator = new Mock<IValidator<User>>();
            _proposalValidator = new Mock<IValidator<Proposal>>();
            _userService = new Mock<IService<User>>();

            _proposalService = new Mock<IService<Proposal>>();
            _authService = new Mock<IAuthService>();

            _service = new ProposalService(_proposalService.Object, _proposalValidator.Object);
        }

        [TestMethod]
        public void CallCreate_WithMalformedProposal_ThenReturnsNull()
        {
            _proposalValidator.Setup(x => x.CanAdd(It.IsAny<Proposal>())).Returns(false);

            var createdProposal = _service.Create(new Proposal());

            Assert.IsNull(createdProposal);
        }

        [TestMethod]
        public void CallCreate_WithGoodParams_ThenReturnsCreatedProposal()
        {
            _proposalValidator.Setup(x => x.CanAdd(It.IsAny<Proposal>())).Returns(true);
            _proposalService.Setup(x => x.Add(It.IsAny<Proposal>())).Returns(getProposal());

            var createdProposal = _service.Create(new Proposal());

            Assert.IsNotNull(createdProposal.Id);
        }

        [TestMethod]
        public void CallAcceptProposal_WithEmptyProposalsList_ThenReturnsNull()
        {
            var acceptedProposal = _service.AcceptProposal(new List<Proposal>() { }, 999, "");

            Assert.IsNull(acceptedProposal);
        }

        [TestMethod]
        public void CallAcceptProposal_WithProposalsList_ThenReturnsNull()
        {
            List<Proposal> proposalsList = new List<Proposal>() {
                new Proposal() { Id = 1, ProposalState = ProposalState.Waiting},
                new Proposal() { Id = 2, ProposalState = ProposalState.Waiting},
                new Proposal() { Id = 3, ProposalState = ProposalState.Waiting}
            };

            var acceptedProposal = _service.AcceptProposal(proposalsList, 2, "Message d'acceptation");

            Assert.AreEqual(ProposalState.Accepted, acceptedProposal.ProposalState);
        }
    }
}